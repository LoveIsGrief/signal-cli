# Dockerfile for automated build of signal-cli
#
# Refer to the signal-cli github pages for detailed Usage.

# Build signal-cli first
FROM openjdk:17-jdk-slim as build

COPY . /tmp/src
WORKDIR /tmp/src

RUN ./gradlew build --no-daemon
RUN ./gradlew installDist --no-daemon
RUN ./gradlew distTar --no-daemon
RUN mkdir /tmp/app
RUN tar xf build/distributions/signal-cli-*.tar -C /tmp/app

# Only use the built app without all the build dependencies
FROM openjdk:17-slim

COPY --from=build /tmp/app/signal-cli* /app

# Where signal-cli stores keys and messages
RUN useradd --create-home signal-cli
USER signal-cli
WORKDIR /home/signal-cli

ENTRYPOINT ["/app/bin/signal-cli"]

